package training.domain;

import lombok.Data;

import java.util.Collection;

@Data
public class TestObjectContainer {
  private final Collection<TestObject> testObjects;
}
