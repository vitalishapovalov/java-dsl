package training.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "test_objects")
@Entity
public class TestObject {

	@Id
	@Column(unique = true, columnDefinition = "serial")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String name;

	@Column
	private Double lat;

	@Column
	private Double lng;
}
