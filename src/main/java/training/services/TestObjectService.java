package training.services;

import training.domain.TestObject;
import training.domain.TestObjectContainer;

public interface TestObjectService {
	/**
	 * Log test object's data to console.
	 *
	 * @param topic additional message on top of the test object logged data
	 * @param testObject test object itself
	 */
	void logObjectToConsole(String topic, TestObject testObject);

	/**
	 * Log test object's container to console.
	 *
	 * @param topic additional message on top of the test object container logged data
	 * @param testObjectContainer test object container itself
	 */
	void logObjectContainerToConsole(String topic, TestObjectContainer testObjectContainer);

	/**
	 * Indicates that object has positive coordinates.
	 *
	 * @param testObject test object itself
	 * @return coordinates are positive
	 */
	boolean isPositive(TestObject testObject);

	/**
	 * Save test object to database.
	 *
	 * @param testObject test object itself
	 * @return saved instance of added object from postgres
	 */
	TestObject saveToDb(TestObject testObject);
}
