package training.services.impl;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import training.domain.TestObject;
import training.domain.TestObjectContainer;
import training.repository.TestObjectRepository;
import training.services.TestObjectService;

@AllArgsConstructor
@Service
@Slf4j(topic = "Test Objects service")
public class TestObjectServiceImpl implements TestObjectService {

	private final TestObjectRepository testObjectRepository;

	@Override
	public void logObjectToConsole(String topic, TestObject testObject) {
		logTopic(topic);
		log.info("Test object '{}' with id '{}'. Cords: {}, {}",
				testObject.getName(), testObject.getId(), testObject.getLat(), testObject.getLng());
	}

	@Override
	public void logObjectContainerToConsole(String topic,
																					TestObjectContainer testObjectContainer) {
		logTopic(topic);
		for (TestObject testObject : testObjectContainer.getTestObjects()) {
			logObjectToConsole(null, testObject);
		}
	}

	@Override
	public TestObject saveToDb(TestObject testObject) {
		return testObjectRepository.save(testObject);
	}

	@Override
	public boolean isPositive(TestObject testObject) {
		return testObject.getLat() > 0
				&& testObject.getLng() > 0;
	}

	private void logTopic(String topic) {
		if (null != topic) {
			log.info(topic);
		}
	}
}
