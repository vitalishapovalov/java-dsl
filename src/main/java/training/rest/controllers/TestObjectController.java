package training.rest.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestParam;
import training.domain.TestObject;
import training.integration.TestObjectGateway;

import java.util.Collections;

@AllArgsConstructor
@Controller
public class TestObjectController {

	private final static String SUCCESS_RESPONSE_BODY = "Success!";

	private final TestObjectGateway testObjectGateway;

	@GetMapping("generateObject")
	public ResponseEntity<String> generateTestObject(@RequestParam("name") String name,
																								 @RequestParam("lat") Double lat,
																								 @RequestParam("lng") Double lng) {
		TestObject testObject = new TestObject();
		testObject.setName(name);
		testObject.setLat(lat);
		testObject.setLng(lng);
		// Simulate collection of objects
		testObjectGateway.generate(Collections.singletonList(testObject));
		return ResponseEntity.status(HttpStatus.OK).body(SUCCESS_RESPONSE_BODY);
	}
}
