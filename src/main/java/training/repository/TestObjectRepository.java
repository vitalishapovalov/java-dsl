package training.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import training.domain.TestObject;

@Repository
public interface TestObjectRepository extends CrudRepository<TestObject, Integer> {
}
