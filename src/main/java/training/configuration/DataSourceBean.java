package training.configuration;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Configuration
public class DataSourceBean {

	private final ApplicationProperties applicationProperties;

	@ConfigurationProperties(prefix = "spring.datasource")
	@Bean
	public DataSource getDataSource() {
		final String postgresUrl = String.format("jdbc:postgresql://%s:5432/%s?stringtype=unspecified",
				applicationProperties.getPostgresHost(), applicationProperties.getPostgresDb());
		return DataSourceBuilder.create()
				.driverClassName("org.postgresql.Driver")
				.url(postgresUrl)
				.username(applicationProperties.getPostgresUser())
				.password(applicationProperties.getPostgresPassword())
				.build();
	}
}
