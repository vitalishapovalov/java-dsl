package training.configuration;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Configuration
public class RabbitConfiguration {

	private static final String QUEUE = "JAVA-DSL.queue";
	private static final String EXCHANGE = "JAVA-DSL.exchange";
	private static final String ROUTING_KEY = "JAVA-DSL.routing";

	private final ApplicationProperties applicationProperties;

	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		connectionFactory.setHost(applicationProperties.getRabbitHost());
		connectionFactory.setUsername(applicationProperties.getRabbitUser());
		connectionFactory.setPassword(applicationProperties.getRabbitPassword());
		return connectionFactory;
	}

	@Bean
	public Exchange exchange() {
		return ExchangeBuilder.topicExchange(EXCHANGE)
				.durable()
				.build();
	}

	@Bean
	public Queue queue() {
		return QueueBuilder.durable(QUEUE)
				.build();
	}

	@Bean
	public Binding binding() {
		return BindingBuilder
				.bind(queue())
				.to(exchange())
				.with(ROUTING_KEY)
				.noargs();
	}

	@Bean
	public SimpleMessageListenerContainer listenerContainer() {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory());
		container.setQueues(queue());
		container.setConcurrentConsumers(3);
		return container;
	}

	@Bean
	public RabbitTemplate template() {
		final RabbitTemplate rabbitTemplate = new RabbitTemplate();
		rabbitTemplate.setConnectionFactory(connectionFactory());
		rabbitTemplate.setExchange(EXCHANGE);
		rabbitTemplate.setRoutingKey(ROUTING_KEY);
		return rabbitTemplate;
	}
}
