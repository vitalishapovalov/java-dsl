package training.configuration;

import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Getter
@Configuration
public class ApplicationProperties {

	private final String rabbitHost = System.getenv("RABBITMQ_HOST");
	private final String rabbitUser = System.getenv("RABBITMQ_USER");
	private final String rabbitPassword = System.getenv("RABBITMQ_PASSWORD");

	private final String postgresHost = System.getenv("POSTGRES_HOST");
	private final String postgresDb = System.getenv("POSTGRES_DB");
	private final String postgresUser = System.getenv("POSTGRES_USER");
	private final String postgresPassword = System.getenv("POSTGRES_PASSWORD");
}
