package training.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.amqp.Amqp;
import org.springframework.integration.dsl.support.Transformers;

import lombok.AllArgsConstructor;
import training.configuration.RabbitConfiguration;

@AllArgsConstructor
@Configuration
public class TestObjectEndpointConsumer {

	private final RabbitConfiguration rabbitConfiguration;

	@Bean
	public IntegrationFlow testObjectEndpointConsumerFlow() {
		return f -> f
				.transform(Transformers.toJson())
				.log("Message published via endpoint")
				.handle(Amqp.outboundAdapter(rabbitConfiguration.template()));
	}
}
