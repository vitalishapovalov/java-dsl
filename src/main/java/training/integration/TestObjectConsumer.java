package training.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.AggregatorSpec;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.PublishSubscribeSpec;
import org.springframework.integration.dsl.amqp.Amqp;
import org.springframework.integration.dsl.support.GenericHandler;
import org.springframework.integration.dsl.support.Transformers;

import lombok.AllArgsConstructor;
import training.configuration.RabbitConfiguration;
import training.domain.TestObject;
import training.domain.TestObjectContainer;
import training.services.TestObjectService;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@AllArgsConstructor
@Configuration
public class TestObjectConsumer {

	private final RabbitConfiguration rabbitConfiguration;

	private final TestObjectService testObjectService;

	@Bean
	public IntegrationFlow testObjectConsumerFlow() {
		return IntegrationFlows.from(Amqp.inboundAdapter(rabbitConfiguration.listenerContainer()))
				.transform(Transformers.fromJson(TestObject[].class))
				.split()
				.channel(c -> c.executor(Executors.newCachedThreadPool()))
				.<TestObject>handle((p, h) -> testObjectService.saveToDb(p))
				.publishSubscribeChannel(this::filterAndLogPositiveFlow)
				.aggregate(this::aggregateObjects)
				.handle(this::logAggregatedObjects)
				.get();
	}

	private PublishSubscribeSpec filterAndLogPositiveFlow(PublishSubscribeSpec publishSubscribeSpec) {
		return publishSubscribeSpec.subscribe(f -> f
				.filter(testObjectService::isPositive)
				.<TestObject>handle((p, h) -> {
					testObjectService.logObjectToConsole("Positive coordinates:", p);
					return null;
				}));
	}

	private AggregatorSpec aggregateObjects(AggregatorSpec aggregator) {
		return aggregator.outputProcessor(group ->
				new TestObjectContainer(group.getMessages()
						.stream()
						.map(message -> (TestObject) message.getPayload())
						.collect(Collectors.toList())));
	}

	private GenericHandler<TestObjectContainer> logAggregatedObjects(TestObjectContainer testObjectContainer,
																																	 Map<String, Object> headers) {
		final String topic = String.format(
				"Test objects container aggregation is complete. Objects quantity: %s. Aggregated objects:",
				testObjectContainer.getTestObjects().size());
		testObjectService.logObjectContainerToConsole(topic, testObjectContainer);
		return null;
	}
}
