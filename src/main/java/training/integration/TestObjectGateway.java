package training.integration;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import training.domain.TestObject;

import java.util.Collection;

@MessagingGateway
public interface TestObjectGateway {

	@Gateway(requestChannel = "testObjectEndpointConsumerFlow.input")
	void generate(Collection<TestObject> testObject);
}
