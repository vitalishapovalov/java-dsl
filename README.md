# Training: Spring Integration Java DSL

Training project to get more experience with RabbitMQ and Spring Integration Java DSL.

## Usage

Application is set up to listen for the RabbitMQ queue and process each message in a following way:

1. Message payload is an array of objects. Split pipe to process each object separately and concurrently.
2. Save object to Postgres DB and pass saved instance further through pipe.
3. Subflow created. It filters objects, so only the objects with positive coordinates will stay, then logs them.
4. Aggregate all of the added objects into special container
5. Log special container

Integration is done via Spring Integration with Java DSL.

### Build & Start

```bash
mvn clean install
```

### Stop

```bash
sh docker-compose-down.sh
```

## Testing

Tests are made by publishing new messages. In order to do this, you should publish a new message via `Rabbit management` web interface or provided web `Endpoint`.

Results of saving to DB can be viewed via `Postgres admin` web interface.

## Web interfaces

Added only for easy-testing of application.

### Postgres admin

You can easily view state of the DB here.

_DB_: `java_dsl`

_Hostname_: `postgres`

_URL_: `http://127.0.0.1:9991`

_Username_: `postgres_training`

_Password_: `postgres_training`

### Rabbit management

You can publish new messages directly to the needed queue here.

_URL_: `http://127.0.0.1:9992`

_Username_: `rabbit_training`

_Password_: `rabbit_training`

#### Example of the message payload

```json
[{"name":"alex1","lat":50.125,"lng":22.12125},{"name":"alex2","lat":40.125,"lng":32.12125},{"name":"alex3","lat":-10.125,"lng":12.12125},{"name":"alex4","lat":60.125,"lng":72.12125}]
```

### Endpoint

Generates a message, based on provided parameters.

Then this message is passed to special channel via `MessagingGateway`.

After this, a special listener for this channel (made with Spring Integration Java DSL's help)
transforms this message to json and passes it to our main app rabbit queue.

_URL_: `http://127.0.0.1:9993/generateObject`

_METHOD_: `GET`

#### Params

_name_: `String`

_lat_: `Double`

_lng_: `Double`

#### Example

```bash
GET http://127.0.0.1:9993/generateObject?name=ExampleName&lat=29.09213&lng=29.13433
```
