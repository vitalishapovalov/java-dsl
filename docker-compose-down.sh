#!/usr/bin/env bash

docker stop $(docker ps -q --filter=name='trainingspringintegrationjavadsl*')
docker rm $(docker ps -a -q --filter=name='trainingspringintegrationjavadsl*')
docker rmi $(docker images -a -q --filter=reference='trainingspringintegrationjavadsl*')
