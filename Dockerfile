FROM openjdk:8-jre-alpine

ARG JAR_FILE

COPY target/${JAR_FILE} /usr/src/java-dsl/java-dsl.jar

WORKDIR /usr/src/java-dsl

CMD ["java", "-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n", "-jar", "java-dsl.jar"]
